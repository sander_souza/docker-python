from flask import Flask, jsonify, abort, request

app = Flask(__name__)

deny = 'Not autorized'

def authkey():
    headers = request.headers
    auth = headers.get("x-Api-Key")
    if auth == '012345678':
        return True
    return False

@app.route('/helloworld', methods=['GET'])
def helloworld():
    auth = authkey()
    if auth:
        stdout = {'id':'Hello World'}
        return jsonify(stdout), 200
    return jsonify({'error':deny}), 401

@app.route('/olamundo', methods=['GET'])
def olamundo():
    auth = authkey()
    if auth:
        stdout = {'id':'Olá Mundo'}
        return jsonify(stdout)
    return jsonify({'error':deny}), 401

app.run(host='0.0.0.0', port=8080, debug=True)